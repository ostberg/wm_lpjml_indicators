# World Modelers LPJmL crop yield indicators

This project contains code to aggregate raw outputs from LPJmL crop yield
forecasts and/or historical yield simulations into indicators that are more
readily usable.

## Functionality
- `average_yield.R`: function `average_yield()` returns an array of crop data
  averaged across time. Spatial and crop dimensions are kept
- `download_yield_data.R`: function `download_yield_data()` attempts to download
  the raw output file for one LPJmL simulation (either historical time series or
  forecast) from the remote server and returns the local file if downloaded
  successfully.
- `get_forecast_options.R`: function `get_forecast_options` returns a list of
  character vectors with elements 'irrigation_options', 'fertilizer_options',
  'sowing_options' giving all available options on the remote server
- `get_weather_options.R`: function `get_weather_options` returns either a
  character vector of weather options available on the remote server for a
  specific combination of irrigation, fertilizer and sowing option, or a list
  containing a character vector of weather options and a character vector with
  corresponding remote file names.
- `rsync_setup.R`: sets up basic information to retrieve data from remote server

## Indicators
### risk_yield_loss.R
This indicator describes the risk that forecasted yields will fall below
historical yield levels by more than a certain percentage threshold. The risk is
calculated based on the fraction of weather options that show the respective
yield loss. The indicator returns the harvested area at risk (where the risk of
yield loss falls within the provided risk range). Depending on setting, the
indicator also returns the gridded risk of yield loss and/or the total harvested
area regardless of risk of yield loss.

User-specified parameters:
- `hist_ref_startyear` and `hist_ref_endyear`: start and end of the historical
  reference period against which forecasts are compared (default: 2016-2020)
- `yield_loss`: yield loss threshold in percent compared to historical reference
  to be counted towards yield loss risk (default: 10)
- `risk_lower` and `risk_higher`: risk threshold in %, i.e. the minimum and
  maximum fraction of all weather options that need to show a yield loss greater
  than the `yield_loss` threshold. If not provided, 5 risk categories are used:
  0-20%, 20-40%, 40-60%, 60-80%, 80-100%
- `forecast_year`: the forecast year to be compared against the historical
  reference yield. If not provided, use all years in forecast outputs.
- `irrigation_scenario`: forecast intervention option (only "reference"
  available for August 2021 and April 2022 experiment)
- `fertilizer_scenario`: forecast intervention option (default: reference)
- `sowing_scenario`: forecast intervention option (default: reference)
- `experiment`: allows select LPJmL results from different experiments
  ("August2021_experiment" and "April2022_experiment" currently available)
- `write_yield_loss_risk`: whether or not to write gridded yield loss risk to
  file (default: TRUE; provided as FALSE in updated model registration)
- `write_country_mask`: whether or not to write country and admin1 name for each
  grid cell to NetCDF files (default:  FALSE, provided as TRUE in updated model
  registration)
- `write_total_harvested_area`: whether to write total harvested area
  (regardless of yield loss) to file (default: FALSE, provided as TRUE in
  updated model registration)
- `irrigation_setting`: optional sub-selection of data, either "rainfed",
  "irrigated" or "rainfed & irrigated". The latter averages over rainfed &
  irirgated. If not provided, no sub-selection is made.
- `crop_setting`: sub-selection of crop. If not provided, results for all crops
  are returned.
  

The script first downloads LPJmL outputs for the historical time period from the
remote server and calculates for each crop and each grid cell the average yield
during the reference period specified by `hist_ref_startyear` and
`hist_ref_endyear`. If `irrigation_setting` and/or `crop_setting` is provided,
the full data are subset.

In a second step, outputs from LPJmL crop yield forecast simulations are
downloaded for all weather options available on the remote server for the
specified combination of `irrigation_scenario`, `fertilizer_scenario` and
`sowing_scenario`. Forecasted yield during the `forecast_year` (or all included
years) is calculated for each crop (or sub-selection or `irrigation_setting` and
`crop_setting`) and grid cell and compared against the historical reference
yield. This risk of yield loss is calculated for each crop and grid cell as the
fraction of weather options that show a yield loss (compared to reference yield)
of at least `yield_loss` percent.

Depending on setting, the script returns one to three output variables:
1. `yield_loss_risk`: The risk that yields fall at least `yield_loss` percent
  below historical reference yield for each crop and grid cell (output can be
  switched off)
2. `harvested_area_at_risk`: Harvested area affected by yield loss of at least
  `yield_loss` percent, grouped into a user-defined risk range or 5 risk
    categories (0-20%, 20-40%, 40-60%, 60-80%, 80-100%), for each (included)
    crop and grid cell.
3. `total_harvested_area`: Total harvested area regardless of yield loss, for
  each (included) crop and grid cell (output can be switched off)
